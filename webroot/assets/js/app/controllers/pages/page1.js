// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            console.log(app.rightCount);    

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.page1_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);
            
            // 是否答过题
            var isAnwser = 0,
                canNext = 0,
                canShowModal = 1;
            function showModal() {
                setTimeout(function() {
                    if(canShowModal == 1) {
                        $('.modal1').fadeIn('fast');
                    };
                }, 300);
            };
            // 选对
            $('.ques1').on('click', function() {
                if(isAnwser == 0) {
                    app.rightCount += 1;
                    $('.ques1').attr('src', 'assets/images/page1/a1.png');
                    isAnwser = 1;
                    $('.btn-next').attr('src', 'assets/images/common/btn-next-blue.png');
                    canNext = 1;
                };
            });
            // 选错
            $('.ques2').on('click', function() {
                if(isAnwser == 0) {
                    $('.ques2').attr('src', 'assets/images/page1/b0.png');
                    isAnwser = 1;
                    $('.btn-next').attr('src', 'assets/images/common/btn-next-blue.png');
                    canNext = 1;
                    showModal();
                };
            });
            $('.ques3').on('click', function() {
                if(isAnwser == 0) {
                    $('.ques3').attr('src', 'assets/images/page1/c0.png');
                    isAnwser = 1;
                    $('.btn-next').attr('src', 'assets/images/common/btn-next-blue.png');
                    canNext = 1;
                    showModal();
                };
            });
            $('.ques4').on('click', function() {
                if(isAnwser == 0) {
                    $('.ques4').attr('src', 'assets/images/page1/d0.png');
                    isAnwser = 1;
                    $('.btn-next').attr('src', 'assets/images/common/btn-next-blue.png');
                    canNext = 1;
                    showModal();
                };
            });

            // 下一题
            $('.btn-next').on('click', function() {
                if(canNext == 1) {
                    gtag('event', 'next1', {'event_category': 'JJ', 'event_label': 'click'});
                    tl.kill();
                    canShowModal = 0;
                    app.router.goto('page2');
                };
            });
        },
        resize: function(ww, wh) {
            if ($(window).height() < 600) {
                if (app.s >= 100) {
                    //
                } else {
                    //
                }
            } else {
                if (app.s >= 100) {
                    //
                } else {
                    //
                }
            }
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
