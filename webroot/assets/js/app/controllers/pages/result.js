// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            console.log(app.rightCount);

            // title
            if(app.rightCount < 2) {
                $('.page-result .title').attr('src', 'assets/images/result/title0.png');
            } else {
                $('.page-result .title').attr('src', 'assets/images/result/title1.png');
            };

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.result_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // to form
            context.$('.btn-form').on('click', function() {
                gtag('event', 'enroll', {'event_category': 'JJ', 'event_label': 'click'});
                tl.kill();
                app.router.goto('form'); 
            });
        },
        resize: function(ww, wh) {
            if ($(window).height() < 600) {
                if (app.s >= 100) {
                    //
                } else {
                    //
                }
            } else {
                if (app.s >= 100) {
                    //
                } else {
                    //
                }
            }
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
