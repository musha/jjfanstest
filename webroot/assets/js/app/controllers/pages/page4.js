// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            console.log(app.rightCount);

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.page4_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // 是否写过
            var canResult = 0;
            $('.reason').bind('input propertychange', function() {
                if($('.reason').val().length > 0) {
                    canResult = 1;
                    $('.btn-result').attr('src', 'assets/images/page4/btn-result-blue.png');
                } else {
                    canResult = 0;
                    $('.btn-result').attr('src', 'assets/images/page4/btn-result-grey.png');
                };
            });

            // 到结果
            $('.btn-result').on('click', function() {
                if(canResult == 1) {
                    gtag('event', 'next4', {'event_category': 'JJ', 'event_label': 'click'});
                    tl.kill();
                    app.reason = $('.reason').val();
                    app.router.goto('result');
                };
            });
        },
        resize: function(ww, wh) {
            if ($(window).height() < 600) {
                if (app.s >= 100) {
                    //
                } else {
                    //
                }
            } else {
                if (app.s >= 100) {
                    //
                } else {
                    //
                }
            }
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
