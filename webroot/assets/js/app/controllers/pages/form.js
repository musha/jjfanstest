// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.form_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // gtag
            $('.userPhone').on('click', function() {
                gtag('event', 'tele', {'event_category': 'JJ', 'event_label': 'click'});
            });
            $('.userTime').on('click', function() {
                gtag('event', 'date', {'event_category': 'JJ', 'event_label': 'click'});
            });
            $('.userNumber').on('click', function() {
                gtag('event', 'order', {'event_category': 'JJ', 'event_label': 'click'});
            });

            // 检查号码
            function checkPhone(phone) {
                if (phone == '') {
                    $('.modal-error1').fadeIn('fast');
                    return;
                };
                // 8或11位
                if (!/^(\d{8}|1[0-9][0-9]\d{8})$/.test(phone)) {
                    $('.modal-error2').fadeIn('fast');
                    return false;
                } else {
                    return true;
                };
            };

            // agreement
            $('.agreement-box .box').on('click', function() {
                if($('.agreement-box').data('check')) {
                    $('.agreement-box').data('check', false);
                    $('.box-img').attr('src', 'assets/images/form/nocheck.png');
                } else {
                    $('.agreement-box').data('check', true);
                    $('.box-img').attr('src', 'assets/images/form/checked.png');
                }
            });

            // agreement modal
            $('.box-text').on('click', function() {
                $('.modal-agreement2').fadeIn('fast');
            });

            // close
            $('.close').on('click', function() {
                $('.modal-agreement2').fadeOut('fast');
            });

            // submit
            var canSubmit = 1; // 是否可以点击提交，默认可以
            $('btn-submit').off('click');
            $('.btn-submit').on('click', function() {
                gtag('event', 'submit', {'event_category': 'JJ', 'event_label': 'click'});
                // console.log($('.userPhone').val());
                // console.log($('.userTime').val());
                // console.log($('.userNumber').val());
                // console.log(app.reason);
                if(checkPhone($('.userPhone').val())) {
                    if($('.agreement-box').data('check')) {
                        if(canSubmit == 1) {
                            // 提交中不能再次提交
                            canSubmit == 0;
                            $.ajax({
                                type: 'POST',
                                url: 'http://changijjlin.gypserver.com/Server/Save',
                                cache: false,
                                // async: false,
                                dataType: 'json',
                                data: {
                                    phone: $('.userPhone').val(),
                                    travelTime: $('.userTime').val(),
                                    orderNum: $('.userNumber').val(),
                                    reasonStr: app.reason
                                },
                                success: function(data) {
                                    console.log(data);
                                    if(data.status) {
                                        $('.modal-success').fadeIn('fast');
                                    } else {
                                        alert(data.msg);
                                    };
                                },
                                error: function(data) {
                                    // todo
                                },
                                complete: function(data) {
                                    // console.log(data);
                                    canSubmit == 1;
                                }
                            }); 
                        };
                    } else {
                        $('.modal-error3').fadeIn('fast');
                    };
                }; 
            });

            // close modal
            $('.modal-success img').on('click', function() {
                $('.modal-success').fadeOut('fast');
                // window.location.href = 'http://www.biying.com';
            });
            $('.modal-error1 img').on('click', function() {
                $('.modal-error1').fadeOut('fast');
            });
            $('.modal-error2 img').on('click', function() {
                $('.modal-error2').fadeOut('fast');
            });
            $('.modal-error3 img').on('click', function() {
                $('.modal-error3').fadeOut('fast');
            });
        },
        resize: function(ww, wh) {
            if ($(window).height() < 600) {
                if (app.s >= 100) {
                    //
                } else {
                    //
                }
            } else {
                if (app.s >= 100) {
                    //
                } else {
                    //
                }
            }
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
