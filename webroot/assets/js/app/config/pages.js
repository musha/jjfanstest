define([
        "controllers/pages/loading",
        "controllers/pages/home",
        "controllers/pages/page1",
        "controllers/pages/page2",
        "controllers/pages/page3",
        "controllers/pages/page4",
        "controllers/pages/result",
        "controllers/pages/form",
    ],
    function(Loading, Home, Page1, Page2, Page3, Page4, Result, Form) {
        var pages = [
            {
                routeId: 'loading',
                type: 'main',
                landing: true,
                page: function() {
                    return new Loading.View({ template: "pages/loading" });
                }
            },
            {
                routeId: 'home',
                type: 'main',
                landing: false,
                page: function() {
                    return new Home.View({ template: "pages/home" });
                }
            },
            {
                routeId: 'page1',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page1.View({ template: "pages/page1" });
                }
            },
            {
                routeId: 'page2',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page2.View({ template: "pages/page2" });
                }
            },
            {
                routeId: 'page3',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page3.View({ template: "pages/page3" });
                }
            },
            {
                routeId: 'page4',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page4.View({ template: "pages/page4" });
                }
            },
            {
                routeId: 'result',
                type: 'main',
                landing: false,
                page: function() {
                    return new Result.View({ template: "pages/result" });
                }
            },
            {
                routeId: 'form',
                type: 'main',
                landing: false,
                page: function() {
                    return new Form.View({ template: "pages/form" });
                }
            },
        ];
        return pages;
    });