require(["app", "managers/layout-manager", "managers/router", "managers/resize-manager", "managers/tracker", "managers/ui-framework7"],


    function(app, LayoutManager, Router, ResizeManager, Tracker, Framework7) {

        // layout manager
        app.layout = (new LayoutManager()).layout();

        // router
        app.router = new Router();

        app.resizeManager = new ResizeManager();

        app.framework7 = new Framework7();

        app.tracker = new Tracker({ gaAccount: "", baiduAccount: "" });

        app.eventBus = _.extend({}, Backbone.Events);

        /*
         app.user = new User.Model();
         app.user.getInfo();
         */

        app.layout.render().promise().done(function() {
            Backbone.history.start({
                pushState: false
            });
        });

        // 数值
        //app.currentPage = ''; // 停留页数
        app.rightCount = 0; // 答对题目数

        // 微信不分享
        // function onBridgeReady() { 
        //     WeixinJSBridge.call('hideOptionMenu'); 
        // };
        // if (typeof WeixinJSBridge == "undefined") { 
        //     if (document.addEventListener) { 
        //         document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false); 
        //     } else if (document.attachEvent) { 
        //         document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
        //         document.attachEvent('onWeixinJSBridgeReady', onBridgeReady); 
        //     } 
        // } else { 
        //     onBridgeReady(); 
        // };

        // console.log(wx_conf);
        // // 微信分享api
        // wx.config({
        //     debug: true,
        //     appId: wx_conf.appId,
        //     timestamp: wx_conf.timestamp,
        //     nonceStr: wx_conf.nonceStr,
        //     signature: wx_conf.signature,
        //     jsApiList: [
        //         'checkJsApi',
        //         'onMenuShareTimeline',
        //         'onMenuShareAppMessage'
        //     ]
        // });
        // //分享标题
        // var wxtitle = 'JJ真爱粉大联考';
        // //分享内容
        // var wxdesc = 'get林俊杰《圣所2.0》世界巡回演唱会新加坡站全球最后2张票+往返机票！';
        // //分享网址
        // var baseurl = '';
        // var shareurl = 'http://changijjlin.gypserver.com';
        // //分享图片
        // var imgUrl = 'http://changijjlin.gypserver.com/assets/images/share.jpg';

        // wx.ready(function() {
        //     var shareData = {
        //         title: wxtitle,
        //         desc: wxdesc,
        //         link: shareurl,
        //         imgUrl: imgUrl,
        //         success: function() {},
        //         error: function(data) {
        //             console.log(data);
        //         }
        //     };
        //     var shareDataline = {
        //         title: wxtitle,
        //         desc: wxdesc,
        //         link: shareurl,
        //         imgUrl: imgUrl,
        //         success: function() {},
        //         error: function(data) {
        //             console.log(data);
        //         }
        //     };
        //     wx.onMenuShareAppMessage(shareData);
        //     wx.onMenuShareTimeline(shareDataline);
        // });

        var shareData = {
            title: 'JJ真爱粉大联考',
            desc: 'get林俊杰《圣所2.0》世界巡回演唱会新加坡站全球最后2张票+往返机票！',
            link: 'http://changijjlin.gypserver.com',
            imgUrl: 'http://changijjlin.gypserver.com/assets/images/share.jpg',
            type:'',
            dataUrl:'',
            success: function() {
                // console.log('-----success'+ shareData.link)
            },
            error: function() {
                //
            },
            cancel: function () { 
              // 用户取消分享后执行的回调函数
            }
        };

        function loadJs(url,callback){
            var script=document.createElement('script');
            script.type="text/javascript";
            if(typeof(callback)!="undefined"){
                if(script.readyState){
                    script.onreadystatechange=function(){
                        if(script.readyState == "loaded" || script.readyState == "complete"){
                            script.onreadystatechange=null;
                            callback();
                        }
                    }
                }else{
                    script.onload=function(){
                        callback();
                    }
                }
            }
            script.src=url;
            document.body.appendChild(script);
        };

        function wxshare(){
            loadJs("//demo2.gypserver.com/weixinShareFile/?url=" + encodeURIComponent(location.href.split('#')[0]),function(){
                // console.log('wx_conf' + JSON.stringify(wx_conf))
                wx.config({
                    debug: false,
                    appId: wx_conf.appId,
                    timestamp: wx_conf.timestamp,
                    nonceStr: wx_conf.nonceStr,
                    signature: wx_conf.signature,
                    jsApiList: [
                        'checkJsApi',
                        'onMenuShareTimeline',
                        'onMenuShareAppMessage'
                    ]
                });   
            });
            wx.ready(function() {
                // console.log('wxshare' + JSON.stringify(shareData))
                wx.onMenuShareAppMessage(shareData);
                wx.onMenuShareTimeline(shareData);    
            });  
            wx.error(function(res){
                console.log('出错了'+JSON.stringify(res))
                loadJs("//demo2.gypserver.com/weixinShareFile/?url=" + encodeURIComponent(location.href.split('#')[0]),function(){
                    // console.log('wx_conf' + JSON.stringify(wx_conf))
                    wx.config({
                        debug: false,
                        appId: wx_conf.appId,
                        timestamp: wx_conf.timestamp,
                        nonceStr: wx_conf.nonceStr,
                        signature: wx_conf.signature,
                        jsApiList: [
                            'checkJsApi',
                            'onMenuShareTimeline',
                            'onMenuShareAppMessage'
                        ]
                    });   
                });
            });
        };
        wxshare();
        
    });