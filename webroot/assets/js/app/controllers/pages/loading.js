// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/zepto.html5Loader.min"],
            function() {
                done();
                // console.log(sessionStorage.getItem('currentPage'));
            });
        },
        afterRender: function() {
            // 略过loading
            if(sessionStorage.getItem('currentPage') != null || sessionStorage.getItem('currentPage') != undefined) {
                $('.loading').hide();
                app.router.goto(sessionStorage.getItem('currentPage'));
                return;
            };
            
            // 获取授权
            // app.localUrl = window.location.href;
            // if(app.localUrl.indexOf('openid') < 0) {
            //     location.href = app.baseUrl + '/interface/WeChatService.ashx?action=oauth&backurl=' + encodeURIComponent(app.localUrl);
            // };
            // app.openid = wxr_getParameterByName('openid');

            var context = this;

            // 动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.loading_page'), 0.2, { autoAlpha: 0, onStart: function() {
                // 预加载
                var firstLoadFiles = {
                    "files": [
                        // {
                        //     "type": "AUDIO",
                        //     "sources": {
                        //         "mp3": {
                        //             "source": "assets/audio/music.mp3",
                        //             "size": 1
                        //         }
                        //     },
                        // },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/bg.jpg",
                            "size": 79872
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/colors.png",
                            "size": 15360
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/ques-box.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/btn-next-blue.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/btn-next-grey.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/agre-box.png",
                            "size": 7168
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/text.png",
                            "size": 176128
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/close.png",
                            "size": 1024
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/loading/lin.gif",
                            "size": 20480
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/banner.png",
                            "size": 74752
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/text.png",
                            "size": 19456
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/hand.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/heart.png",
                            "size": 1024
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/agreement.png",
                            "size": 998
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/btn-start.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/title.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/modal.png",
                            "size": 7168
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/a.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/a1.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/b.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/b0.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/c.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/c0.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/d.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/d0.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/title.png",
                            "size": 6144
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/modal.png",
                            "size": 7168
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/a.png",
                            "size": 8192
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/a0.png",
                            "size": 8192
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/b.png",
                            "size": 10240
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/b0.png",
                            "size": 10240
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/c.png",
                            "size": 12288
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/c1.png",
                            "size": 12288
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/d.png",
                            "size": 8192
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/d0.png",
                            "size": 8192
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/title.png",
                            "size": 7168
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/modal.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/poster.jpg",
                            "size": 22528
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/a.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/a0.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/b.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/b1.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/c.png",
                            "size": 1024
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/c0.png",
                            "size": 1024
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/d.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/d0.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page4/title.png",
                            "size": 9216
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page4/ques-box.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page4/btn-result-blue.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page4/btn-result-grey.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result/title0.png",
                            "size": 8192
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result/title1.png",
                            "size": 8192
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result/gift-box.png",
                            "size": 22528
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result/btn-form.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/title.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/input.png",
                            "size": 1024
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/phone.png",
                            "size": 940
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/time.png",
                            "size": 958
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/number.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/gift.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/btn-submit.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/modal.png",
                            "size": 29696
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/error1.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/error2.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/error3.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/checked.png",
                            "size":6144
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/form/nocheck.png",
                            "size": 6144
                        },
                    ]
                };
                $.html5Loader({
                    filesToLoad: firstLoadFiles,
                    onBeforeLoad: function() {},
                    onElementLoaded: function(obj, elm) {},
                    onUpdate: function(percentage) {
                        // console.log(percentage);
                        $('.bar-blue').css({'width': percentage + '%' });
                        $('.num').html(percentage);
                    },
                    onComplete: function() {
                        setTimeout(function() {
                            tl.kill();
                            app.router.goto('home');
                        }, 1500);
                    }
                });
            }, onComplete: function() {
                //
            } }, 0);  
        }
    })
    //  Return the module for AMD compliance.
    return Page;
});
