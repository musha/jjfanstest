// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            sessionStorage.setItem('currentPage', 'home');

            var context = this;

            // 动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.home_page'), 0.2, { autoAlpha: 0, onComplete: function() {
                heart();
                shake(context.$('.hand'), 'rotate15', 0, 0.3, 0.3);
            } }, 0.1);

            //心
            function heart() {
                var tl = new TimelineMax({ repeat: -1, repeatDelay: 0.4 });
                tl.to(context.$('.heart'), 0.6, { autoAlpha: 0, 'top': '48%', 'right': '9.3%', ease: Power0.easeNone }, 0.1);
            };

            // 图标晃动
            function shake(el, className, start, end, delay) {
                var tl = new TimelineMax({repeat: -1, repeatDelay: delay });
                tl.to(el, 0, { className: '+=' + className }, start);
                tl.to(el, 0, { className: '-=' + className }, end);
            };

            // start
            $('.btn-start').on('click', function() {
                gtag('event', 'start', {'event_category': 'JJ', 'event_label': 'click'});
                tl.kill();
                app.router.goto('page1');
            });

            // agreement
            $('.agreement').on('click', function() {
                $('.modal-agreement').fadeIn('fast');
            });

            //close
            $('.close').on('click', function() {
                $('.modal-agreement').fadeOut('fast');
            });
        }
    })
    //  Return the module for AMD compliance.
    return Page;
})
